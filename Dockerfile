FROM golang:1.12
WORKDIR /src
COPY . /src
RUN go build -o golang-chat-ui

EXPOSE 8080
CMD ["./golang-chat-ui"]