function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

new Vue({
    el: '#app',
    // uuid: require('uuid'),


    data: {
        ws: null, // Our websocket
        newMsg: '', // Holds new messages to be sent to the server
        chatContent: '', // A running list of chat messages displayed on the screen
        userId: null, // user id address used for grabbing an avatar
        chatId: null, // user id address used for grabbing an avatar
        action: null, // Our username
        joined: false // True if user id and username have been filled in
    },
    created: function() {
        var urlParams = new URLSearchParams(window.location.search);
        var self = this;
        this.userId = urlParams.get('userID');
        this.chatId = urlParams.get('chatID');
        url = 'ws://ec2-35-161-7-115.us-west-2.compute.amazonaws.com/ws?userID=' + this.userId + "&chatID=" +this.chatId
        // url = 'ws://chat.cocre8.co/ws?userID=' + this.userId + "&chatID=" +this.chatId
        // url = 'ws://localhost:3000/ws?userID=' + this.userId + "&chatID=" +this.chatId
        // url = 'ws://websocket.cocre8.co///ws?userID=' + this.userId + "&chatID=" +this.chatId
        this.ws = new WebSocket(url);
        this.ws.addEventListener('message', function(e) {
            var msg = JSON.parse(e.data);
            console.log(msg);
            self.chatContent += '<div class="chip">'
                    + '<img src="' + self.gravatarURL(msg.sender) + '">' // Avatar
                    + msg.sender
                + '</div>'
                + emojione.toImage(msg.text) + '<br/>'; // Parse emojis

            var element = document.getElementById('chat-messages');
            element.scrollTop = element.scrollHeight; // Auto scroll to the bottom
        });
    },
    methods: {
        send: function () {
            if (this.newMsg != '') {
                this.ws.send(
                    JSON.stringify({
                        // id: generateUUID(),
                            chatId: this.chatId,
                            sender: this.userId,
                            action: "create",
                            text: $('<p>').html(this.newMsg).text() // Strip out html
                    }
                ));
                this.newMsg = ''; // Reset newMsg
            }
        },

        join: function () {
            // if (!this.chatId) {
            //     Materialize.toast('You must enter the id', 2000);
            //     return
            // }
            // this.chatId = $('<p>').html(this.chatId).text();
            this.joined = true;
        },
        gravatarURL: function(userId) {
            return 'http://www.gravatar.com/avatar/' + CryptoJS.MD5(userId);
        }
    }
});